package io.axual.training.common;

import io.axual.payments.AccountId;
import io.axual.payments.IBAN;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

public class Account {
    private static final int NUM_ACCOUNTS = 20;
    private static final BigDecimal MIN_BALANCE = new BigDecimal("-500");
    private static final BigDecimal MAX_BALANCE = new BigDecimal("4000");

    private AccountId identification;
    private String name;
    private BigDecimal balance = new BigDecimal("1000");

    public Account(AccountId identification, String name) {
        this.identification = identification;
        this.name = name;
    }

    public AccountId getIdentification() {
        return identification;
    }

    public String getName() {
        return name;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public boolean canDebitAmount(BigDecimal amount) {
        return AmountUtils.greaterThanOrEquals(balance.subtract(amount), MIN_BALANCE);
    }

    public boolean canCreditAmount(BigDecimal amount) {
        return AmountUtils.smallerThan(balance.add(amount), MAX_BALANCE);
    }

    public static List<Account> getAll() {
        List<Account> accountList = new ArrayList<>();
        for (int i = 0; i < NUM_ACCOUNTS; i++) {
            String name = "Account #" + i;
            IBAN iban = IBAN.newBuilder()
                    .setAccountNumber(format("NL%02dRABO%s", i % 100, format("%010d", i)))
                    .build();
            AccountId identification = AccountId.newBuilder()
                    .setId(iban)
                    .build();
            accountList.add(new Account(identification, name));
        }
        return accountList;
    }
}
